use alloc::{vec, vec::Vec};
use anyhow::anyhow;
use slint::{
    platform::{PointerEventButton, WindowEvent},
    LogicalPosition,
};
use uefi::{boot::ScopedProtocol, proto::console::pointer::Pointer};

pub(crate) struct MouseWrapper {
    protocol: ScopedProtocol<Pointer>,
    x: f32,
    y: f32,
    left: bool,
    right: bool,
}

impl MouseWrapper {
    pub(crate) fn new(protocol: ScopedProtocol<Pointer>) -> Self {
        Self {
            protocol,
            x: 0.0,
            y: 0.0,
            left: false,
            right: false,
        }
    }

    pub(crate) fn get_events(&mut self) -> anyhow::Result<Vec<WindowEvent>> {
        let mut events = vec![];

        let Some(state) = self
            .protocol
            .read_state()
            .map_err(|e| anyhow!("Key read error: {:?}", e))?
        else {
            return Ok(events);
        };

        let [dx, dy, _] = state.relative_movement;

        #[allow(clippy::cast_precision_loss)]
        {
            self.x += dx as f32;
            self.y += dy as f32;
        };

        let position = LogicalPosition::new(self.x, self.y);

        if dx != 0 || dy != 0 {
            events.push(WindowEvent::PointerMoved { position });
        }

        let [left, right] = state.button;

        if !self.left && left {
            events.push(WindowEvent::PointerPressed {
                position,
                button: PointerEventButton::Left,
            });
        };

        if self.left && !left {
            events.push(WindowEvent::PointerReleased {
                position,
                button: PointerEventButton::Left,
            });
        };

        if !self.right && right {
            events.push(WindowEvent::PointerPressed {
                position,
                button: PointerEventButton::Right,
            });
        };

        if self.right && !right {
            events.push(WindowEvent::PointerReleased {
                position,
                button: PointerEventButton::Right,
            });
        };

        self.left = left;
        self.right = right;

        Ok(events)
    }
}
