use anyhow::anyhow;
use slint::platform::Key as SlintKey;
use uefi::{
    boot::ScopedProtocol,
    proto::console::text::{Input, Key as UefiKey, ScanCode},
};

pub(crate) struct KeyboardWrapper {
    protocol: ScopedProtocol<Input>,
}

impl KeyboardWrapper {
    pub(crate) fn new(protocol: ScopedProtocol<Input>) -> Self {
        Self { protocol }
    }

    pub(crate) fn get_event(&mut self) -> anyhow::Result<Option<char>> {
        let maybe_key = self
            .protocol
            .read_key()
            .map_err(|e| anyhow!("Key read error: {:?}", e))?;

        Ok(maybe_key.and_then(|k| match k {
            UefiKey::Printable(c) => Some(match c.into() {
                // windows-ass protocol
                '\r' => '\n',
                c => c,
            }),
            UefiKey::Special(sk) => match sk {
                ScanCode::UP => Some(SlintKey::UpArrow),
                ScanCode::DOWN => Some(SlintKey::DownArrow),
                ScanCode::LEFT => Some(SlintKey::LeftArrow),
                ScanCode::RIGHT => Some(SlintKey::RightArrow),
                _ => None,
            }
            .map(core::convert::Into::into),
        }))
    }
}
