use anyhow::anyhow;
use log::debug;
use uefi::{boot::ScopedProtocol, proto::Protocol};

pub(crate) fn get_protocol<T: Protocol>() -> anyhow::Result<ScopedProtocol<T>> {
    debug!("Getting protocol handle...");

    let handle = uefi::boot::get_handle_for_protocol::<T>()
        .map_err(|e| anyhow!("Protocol get error: {:?}", e))?;

    debug!("Getting protocol instance...");

    let instance = uefi::boot::open_protocol_exclusive::<T>(handle)
        .map_err(|e| anyhow!("Protocol open error: {:?}", e))?;

    Ok(instance)
}
