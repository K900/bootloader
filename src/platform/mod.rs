use core::{
    cell::RefCell,
    sync::atomic::{AtomicBool, Ordering},
};

use alloc::{boxed::Box, rc::Rc, sync::Arc};
use chrono::{DateTime, Utc};
use log::{debug, warn};
use slint::{
    platform::{software_renderer::MinimalSoftwareWindow, EventLoopProxy, Platform, WindowEvent},
    EventLoopError, PhysicalSize, PlatformError,
};
use uefi::proto::console::{gop::GraphicsOutput, pointer::Pointer, text::Input};

use self::{graphics::GraphicsWrapper, keyboard::KeyboardWrapper, mouse::MouseWrapper};

pub(crate) mod graphics;
pub(crate) mod keyboard;
pub(crate) mod mouse;
pub(crate) mod time;
pub(crate) mod utils;

type UefiWindow = Rc<MinimalSoftwareWindow>;

pub(crate) struct UefiPlatform {
    graphics: RefCell<GraphicsWrapper>,
    keyboard: RefCell<KeyboardWrapper>,
    mouse: RefCell<MouseWrapper>,

    start: DateTime<Utc>,
    // FIXME: consider double buffering
    window: UefiWindow,

    should_stop: Arc<AtomicBool>,
}

struct UefiEventLoopProxy {
    stop: Arc<AtomicBool>,
}

impl UefiEventLoopProxy {
    fn new(stop: Arc<AtomicBool>) -> Self {
        UefiEventLoopProxy { stop }
    }
}

impl EventLoopProxy for UefiEventLoopProxy {
    fn quit_event_loop(&self) -> Result<(), EventLoopError> {
        self.stop.store(true, Ordering::SeqCst);
        Ok(())
    }

    fn invoke_from_event_loop(
        &self,
        _event: Box<dyn FnOnce() + Send>,
    ) -> Result<(), EventLoopError> {
        panic!("Not supported!")
    }
}

impl UefiPlatform {
    pub(crate) fn new(window: UefiWindow) -> anyhow::Result<Self> {
        let protocol = utils::get_protocol::<GraphicsOutput>()?;
        let graphics = GraphicsWrapper::new(protocol);

        let protocol = utils::get_protocol::<Input>()?;
        let keyboard = KeyboardWrapper::new(protocol);

        let protocol = utils::get_protocol::<Pointer>()?;
        let mouse = MouseWrapper::new(protocol);

        let now = time::get_time();

        Ok(Self {
            graphics: RefCell::new(graphics),
            keyboard: RefCell::new(keyboard),
            mouse: RefCell::new(mouse),
            start: now,
            window,
            should_stop: Arc::new(false.into()),
        })
    }
}

impl Platform for UefiPlatform {
    fn create_window_adapter(
        &self,
    ) -> Result<Rc<(dyn slint::platform::WindowAdapter + 'static)>, PlatformError> {
        Ok(self.window.clone())
    }

    fn duration_since_start(&self) -> core::time::Duration {
        (time::get_time() - self.start)
            .to_std()
            .expect("Time overflow!")
    }

    fn debug_log(&self, arguments: core::fmt::Arguments) {
        debug!("{}", arguments);
    }

    fn run_event_loop(&self) -> Result<(), PlatformError> {
        let mut graphics = self.graphics.borrow_mut();
        let mut keyboard = self.keyboard.borrow_mut();
        let mut mouse = self.mouse.borrow_mut();

        let (width, height) = graphics.resolution();

        #[allow(clippy::cast_possible_truncation)]
        self.window.set_size(PhysicalSize {
            width: width as u32,
            height: height as u32,
        });

        while !self.should_stop.load(Ordering::SeqCst) {
            slint::platform::update_timers_and_animations();

            self.window.draw_if_needed(|renderer| {
                debug!("Repaint!");
                renderer.render(graphics.buffer_mut(), width);

                if let Err(e) = graphics.flip() {
                    warn!("Error when blitting: {:?}", e);
                }
            });

            if let Ok(Some(ch)) = keyboard.get_event() {
                debug!("Pressed: {:?}", ch);
                self.window
                    .dispatch_event(WindowEvent::KeyPressed { text: ch.into() });
                self.window
                    .dispatch_event(WindowEvent::KeyReleased { text: ch.into() });
            }

            if let Ok(events) = mouse.get_events() {
                for event in events {
                    debug!("Mouse event: {:?}", event);
                    self.window.dispatch_event(event);
                }
            }
        }

        Ok(())
    }

    fn new_event_loop_proxy(
        &self,
    ) -> Option<alloc::boxed::Box<dyn slint::platform::EventLoopProxy>> {
        Some(Box::new(UefiEventLoopProxy::new(self.should_stop.clone())))
    }
}
