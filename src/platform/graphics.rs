use alloc::{vec, vec::Vec};
use bytemuck::TransparentWrapper;
use slint::platform::software_renderer::TargetPixel;
use uefi::{
    boot::ScopedProtocol,
    proto::console::gop::{BltOp, BltPixel, BltRegion, GraphicsOutput},
};

#[repr(transparent)]
#[derive(Clone, Copy, Debug)]
pub(crate) struct UefiPixel(pub BltPixel);

// SAFETY: https://docs.rs/bytemuck/latest/bytemuck/trait.TransparentWrapper.html#safety
unsafe impl TransparentWrapper<BltPixel> for UefiPixel {}

impl TargetPixel for UefiPixel {
    #[allow(clippy::cast_possible_truncation)]
    fn blend(&mut self, color: slint::platform::software_renderer::PremultipliedRgbaColor) {
        let a = u16::from(u8::MAX - color.alpha);
        self.0 = BltPixel::new(
            (u16::from(self.0.red) * a / 255) as u8 + color.red,
            (u16::from(self.0.green) * a / 255) as u8 + color.green,
            (u16::from(self.0.blue) * a / 255) as u8 + color.blue,
        );
    }

    fn from_rgb(red: u8, green: u8, blue: u8) -> Self {
        Self(BltPixel::new(red, green, blue))
    }
}

impl Default for UefiPixel {
    fn default() -> Self {
        Self(BltPixel::new(0, 0, 0))
    }
}

pub(crate) struct GraphicsWrapper {
    protocol: ScopedProtocol<GraphicsOutput>,
    resolution: (usize, usize),
    buffer: Vec<UefiPixel>,
}

impl GraphicsWrapper {
    pub(crate) fn new(protocol: ScopedProtocol<GraphicsOutput>) -> Self {
        let resolution = protocol.current_mode_info().resolution();

        Self {
            protocol,
            resolution,
            buffer: vec![UefiPixel::default(); resolution.0 * resolution.1],
        }
    }

    pub(crate) fn resolution(&self) -> (usize, usize) {
        self.resolution
    }

    pub(crate) fn buffer_mut(&mut self) -> &mut [UefiPixel] {
        &mut self.buffer
    }

    pub(crate) fn flip(&mut self) -> Result<(), uefi::Error> {
        self.protocol.blt(BltOp::BufferToVideo {
            buffer: TransparentWrapper::peel_slice(&self.buffer),
            src: BltRegion::Full,
            dest: (0, 0),
            dims: self.resolution,
        })
    }
}
