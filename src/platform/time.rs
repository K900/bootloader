use chrono::{FixedOffset, TimeZone, Utc};

pub(crate) fn get_time() -> chrono::DateTime<Utc> {
    let efi_time = uefi::runtime::get_time().expect("Failed to get time!");

    let offset = match efi_time.time_zone() {
        None => 0,
        Some(offset) => i32::from(offset) * 60,
    };

    FixedOffset::east_opt(offset)
        .unwrap()
        .with_ymd_and_hms(
            i32::from(efi_time.year()),
            u32::from(efi_time.month()),
            u32::from(efi_time.day()),
            u32::from(efi_time.hour()),
            u32::from(efi_time.minute()),
            u32::from(efi_time.second()),
        )
        .unwrap()
        .with_timezone(&Utc)
}
