#![no_main]
#![no_std]
#![warn(clippy::pedantic)]

use anyhow::anyhow;
use log::{error, info};

extern crate alloc;
use alloc::boxed::Box;
use slint::{
    platform::software_renderer::{MinimalSoftwareWindow, RepaintBufferType},
    ComponentHandle,
};
use uefi::{
    prelude::entry,
    runtime::{self, ResetType},
    Status,
};

use crate::platform::UefiPlatform;

#[allow(clippy::all)]
#[allow(clippy::pedantic)]
mod ui {
    slint::include_modules!();
}
mod platform;

fn real_main() -> anyhow::Result<()> {
    info!("Setting up UEFI services...");

    uefi::helpers::init().map_err(|e| anyhow!("Error setting up UEFI services: {:?}", e))?;

    info!("Setting up platform...");

    let window = MinimalSoftwareWindow::new(RepaintBufferType::ReusedBuffer);
    let platform = Box::new(UefiPlatform::new(window)?);
    slint::platform::set_platform(platform).unwrap(); // panics iff platform is already set

    info!("Setting up UI...");

    let ui = ui::BootMenu::new().map_err(|e| anyhow!("Error creating window: {:?}", e))?;

    let bl = ui.global::<ui::Bootloader>();
    bl.set_entries(
        [
            ui::BootEntryGroup {
                name: "NixOS".into(),
                kind: "nixos".into(),
                entries: [ui::BootEntry {
                    uid: "nixos-1".into(),
                    name: "NixOS".into(),
                }]
                .into(),
            },
            ui::BootEntryGroup {
                name: "NixOS but rainbow".into(),
                kind: "nixos-but-rainbow".into(),
                entries: [ui::BootEntry {
                    uid: "nixos-2".into(),
                    name: "NixOS but rainbow".into(),
                }]
                .into(),
            },
        ]
        .into(),
    );

    bl.set_tools(
        [
            ui::BootEntryGroup {
                name: "NixOS".into(),
                kind: "nixos".into(),
                entries: [ui::BootEntry {
                    uid: "nixos-1".into(),
                    name: "NixOS".into(),
                }]
                .into(),
            },
            ui::BootEntryGroup {
                name: "NixOS but rainbow".into(),
                kind: "nixos-but-rainbow".into(),
                entries: [ui::BootEntry {
                    uid: "nixos-2".into(),
                    name: "NixOS but rainbow".into(),
                }]
                .into(),
            },
        ]
        .into(),
    );

    bl.on_boot(|uid| {
        info!("Selected: {}", uid);
    });

    bl.on_reboot(|| {
        info!("Selected reboot");
        runtime::reset(ResetType::WARM, Status::SUCCESS, None);
    });

    bl.on_shutdown(|| {
        info!("Selected shutdown");
        runtime::reset(ResetType::SHUTDOWN, Status::SUCCESS, None);
    });

    ui.run()
        .map_err(|e| anyhow!("Error in event loop: {:?}", e))
}

#[entry]
fn main() -> Status {
    match real_main() {
        Ok(()) => Status::SUCCESS,
        Err(e) => {
            error!("Error: {:?}", e);
            uefi::boot::stall(10_000_000);
            Status::ABORTED
        }
    }
}
