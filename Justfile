run: (_run "dev" "debug")
run-fast: (_run "release" "release")

_run profile outdir: (_build profile)
    uefi-run --bios-path="${OVMF}/FV/OVMF.fd" --boot target/x86_64-unknown-uefi/{{outdir}}/bootloader.efi --size 128 -- -serial stdio

_build profile:
    cargo build --profile {{profile}}
