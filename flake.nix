{
  description = "Here be heinous crimes";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable-small";

    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      self,
      fenix,
      nixpkgs,
      pre-commit-hooks,
      ...
    }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;

      crossToolchain =
        with fenix.packages.x86_64-linux;
        combine [
          stable.rustc
          stable.cargo
          stable.clippy
          stable.rustfmt
          targets.x86_64-unknown-uefi.stable.rust-std
          stable.rust-src
        ];
    in
    {
      devShells.x86_64-linux.default = pkgs.mkShell {
        RUST_SRC_PATH = "${crossToolchain}/lib/rustlib/src/";

        OVMF = pkgs.OVMF.fd;

        LD_LIBRARY_PATH = "${pkgs.fontconfig.lib}/lib";

        packages = [
          crossToolchain
          pkgs.cargo-outdated

          pkgs.just

          pkgs.uefi-run
          pkgs.qemu_kvm

          pkgs.pkg-config
          pkgs.fontconfig
        ];

        inherit (self.checks.x86_64-linux.pre-commit-check) shellHook;
      };

      checks.x86_64-linux.pre-commit-check = pre-commit-hooks.lib.x86_64-linux.run {
        src = ./.;
        hooks = {
          nixfmt-rfc-style.enable = true;
          deadnix.enable = true;
          statix.enable = true;

          rustfmt.enable = true;
          clippy.enable = true;
          cargo-check.enable = true;
        };
        tools = {
          cargo = crossToolchain;
          clippy = crossToolchain;
        };
      };
    };
}
